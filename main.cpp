#include "raylib.h"

int main(int argc, char const *argv[])
{
    // I want a window!!

    // different ways of initialization
    float root_beer = 1.99;
    double cheese_burger{5.99};
    bool shouldHaveLunch{};

    // basic variable review
    int width{350};
    int height{200};
    InitWindow(width, height, "Donovan's Window");

    // raylib uses double buffering
    /*
        we need this loop to keep the window open. It closes
        immediately.
    */ 

   int circle_x{width/2};
   int circle_y{height/2};
   int radius = 25;
    
    SetTargetFPS(60);
   // keep the window open, use this function from raylib   
    while(!WindowShouldClose())
    {
        BeginDrawing();
        // need to do this to avoid flickering
        ClearBackground(WHITE);

        // this draws a circle in the center of the screen.
        // I should probably define the diameter in a var, too.
        DrawCircle(circle_x, circle_y, 25, BLUE);


        // check for user input
        if (IsKeyDown(KEY_D) && circle_x < (width - radius))
        {
            circle_x += 10;
        }

        if (IsKeyDown(KEY_A) && circle_x > (0 + radius))
        {
            circle_x -= 10;
        }


        // frame buffer - image under construction behind the scenes.
        // default frame buffer is black, which causes the flickering.
        EndDrawing();
    }
}
